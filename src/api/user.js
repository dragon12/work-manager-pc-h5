import { baseAPI } from '@/utils/request'
import request from '@/utils/request'

export function login(data) {
  return baseAPI.post('user/public/login', data)
}

export function getInfo(token) {
  return baseAPI.get('user/profile/userInfo')
}

export function logout() {
  return baseAPI.post('user/public/logout')
}

export function getUserList(obj) {
  return request({
    url: 'user/user/index',
    method: 'get',
    params: obj
  })
}

export function getUserInfo(id) {
  return baseAPI.get('user/user/detail?id=' + id)
}

export function saveUserinfo(data) {
  return baseAPI.post('user/user/save', data)
}

export function getDictionary(obj) {
  return request({
    url: 'user/dictionary/query',
    method: 'get',
    params: obj
  })
}
