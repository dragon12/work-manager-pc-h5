import request from '@/utils/request'

export function geList(obj) {
  return request({
    url: 'user/order/refund_list',
    method: 'get',
    params: obj
  })
}
export function refundAudit(obj) {
  return request({
    url: 'user/order/refund_audit',
    method: 'post',
    data: obj
  })
}
