import request from '@/utils/request'

export function getList(obj) {
  return request({
    url: 'user/condominium/index',
    method: 'get',
    params: obj
  })
}
export function getDetails(obj) {
  return request({
    url: 'user/condominium/detail',
    method: 'get',
    params: obj
  })
}
export function save(obj) {
  return request({
    url: 'user/condominium/save',
    method: 'post',
    data: obj
  })
}
export function del(obj) {
  return request({
    url: 'user/condominium/del',
    method: 'post',
    data: obj
  })
}
