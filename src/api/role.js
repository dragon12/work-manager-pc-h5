import request from '@/utils/request'
import { baseAPI } from '../utils/request'

export function getRoutes() {
  return baseAPI.get('/role/routes')
}

export function getRoles() {
  return baseAPI.get('user/role/index')
}

export function updateRole(id, data) {
  return request({
    url: `/vue-element-admin/role/${id}`,
    method: 'put',
    data
  })
}

export function deleteRole(id) {
  return request({
    url: `/vue-element-admin/role/${id}`,
    method: 'delete'
  })
}
