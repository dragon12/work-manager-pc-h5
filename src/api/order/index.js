import request from '@/utils/request'

export function getorderinfo(obj) {
  return request({
    url: 'user/order/index',
    method: 'get',
    params: obj
  })
}
export function getdictionary(obj) {
  return request({
    url: 'user/dictionary/query',
    method: 'get',
    params: obj
  })
}
export function getdetails(obj) {
  return request({
    url: 'user/order/detail',
    method: 'get',
    params: obj
  })
}
export function saveorder(obj) {
  return request({
    url: 'user/order/save',
    method: 'post',
    data: obj
  })
}
export function deleteorder(obj) {
  return request({
    url: 'user/order/delete',
    method: 'post',
    data: obj
  })
}
