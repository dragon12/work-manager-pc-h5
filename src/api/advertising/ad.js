import request from '@/utils/request'

export function getList(obj) {
  return request({
    url: 'user/adv/index',
    method: 'get',
    params: obj
  })
}
export function getDetails(obj) {
  return request({
    url: 'user/adv/detail',
    method: 'get',
    params: obj
  })
}
export function save(obj) {
  return request({
    url: 'user/adv/save',
    method: 'post',
    data: obj
  })
}
export function del(obj) {
  return request({
    url: 'user/adv/del',
    method: 'post',
    data: obj
  })
}
export function getdictionary(obj) {
  return request({
    url: 'user/dictionary/query',
    method: 'get',
    params: obj
  })
}
