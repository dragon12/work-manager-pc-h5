import request from '@/utils/request'

export function getList(obj) {
  return request({
    url: 'user/advposition/index',
    method: 'get',
    params: obj
  })
}
export function getDetails(obj) {
  return request({
    url: 'user/advposition/detail',
    method: 'get',
    params: obj
  })
}
export function Add(obj) {
  return request({
    url: 'user/advposition/save',
    method: 'post',
    data: obj
  })
}
export function del(obj) {
  return request({
    url: 'user/advposition/del',
    method: 'post',
    data: obj
  })
}
