import request from '@/utils/request'

export function getorderinfo(obj) {
  return request({
    url: 'user/mall_order/index',
    method: 'get',
    params: obj
  })
}
export function getdictionary(obj) {
  return request({
    url: 'user/dictionary/query',
    method: 'get',
    params: obj
  })
}
export function getdetails(obj) {
  return request({
    url: 'user/mall_order/detail',
    method: 'get',
    params: obj
  })
}
export function editAddress(obj) {
  return request({
    url: 'user/mall_order/edit',
    method: 'post',
    data: obj
  })
}
export function deleteorder(obj) {
  return request({
    url: 'user/mall_order/del',
    method: 'post',
    data: obj
  })
}
export function refundOrder(obj) {
  return request({
    url: 'user/order/user_order_refund',
    method: 'post',
    data: obj
  })
}
