import { baseAPI } from '@/utils/request'

export function getMessageList(data) {
  return baseAPI.get('user/message/index?keyword=' + data.keyword + '&page=' + data.page + '&time_start=' + data.startDate + '&time_end=' + data.endDate)
}

export function readMessageinfo(data) {
  return baseAPI.post('user/message/read', data)
}
