import request from '@/utils/request'

export function getHomeTag(obj) {
  return request({
    url: 'user/data/total',
    method: 'get',
    params: obj
  })
}
