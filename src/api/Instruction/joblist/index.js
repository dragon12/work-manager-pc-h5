import request from '@/utils/request'

export function getjobinfo(obj) {
  return request({
    url: 'user/wi/index',
    method: 'get',
    params: obj
  })
}
export function getdetail(obj) {
  return request({
    url: 'user/wi/detail',
    method: 'get',
    params: obj
  })
}
export function getdictionary(obj) {
  return request({
    url: 'user/dictionary/query',
    method: 'get',
    params: obj
  })
}
export function savejob(obj) {
  return request({
    url: 'user/wi/save',
    method: 'post',
    data: obj
  })
}
export function deletejob(obj) {
  return request({
    url: 'user/wi/delete',
    method: 'post',
    data: obj
  })
}
export function deletewistep(obj) {
  return request({
    url: 'user/wi/wi_step_delete',
    method: 'post',
    data: obj
  })
}
