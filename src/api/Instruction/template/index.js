import request from '@/utils/request'

export function getwitemplate(obj) {
  return request({
    url: 'user/witemplate/index',
    method: 'get',
    params: obj
  })
}
export function getwitemplatedetail(obj) {
  return request({
    url: 'user/witemplate/detail',
    method: 'get',
    params: obj
  })
}
export function gettemplatetype(obj) {
  return request({
    url: 'user/dictionary/query',
    method: 'get',
    params: obj
  })
}
export function savewitemplate(obj) {
  return request({
    url: 'user/witemplate/save',
    method: 'post',
    data: obj
  })
}
export function deletewitemplate(obj) {
  return request({
    url: 'user/witemplate/delete',
    method: 'post',
    data: obj
  })
}
