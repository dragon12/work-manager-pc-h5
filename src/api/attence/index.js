import request from '@/utils/request'

export function getAttence(obj) {
  return request({
    url: 'user/attence/index',
    method: 'get',
    params: obj
  })
}
