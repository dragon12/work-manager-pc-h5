import request from '@/utils/request'

// 客户联系人查询
export function getContactList(obj) {
  return request({
    url: 'user/hotel/hotel_contact',
    method: 'get',
    params: obj
  })
}
// 客户联系人添加编辑
export function saveContact(obj) {
  return request({
    url: 'user/hotel/hotel_contact_save',
    method: 'post',
    data: obj
  })
}
// 客户联系人删除
export function deleteContact(obj) {
  return request({
    url: 'user/hotel/hotel_contact_delete',
    method: 'post',
    data: obj
  })
}

