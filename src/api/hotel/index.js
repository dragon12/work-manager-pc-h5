import request from '@/utils/request'

export function getHotels(obj) {
  return request({
    url: 'user/hotel/index',
    method: 'get',
    params: obj
  })
}
export function saveHotels(obj) {
  return request({
    url: 'user/hotel/save',
    method: 'post',
    data: obj
  })
}
export function deleteHotel(obj) {
  return request({
    url: 'user/hotel/delete',
    method: 'post',
    data: obj
  })
}
