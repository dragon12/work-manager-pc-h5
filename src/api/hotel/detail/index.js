import request from '@/utils/request'

// 工单查询
export function getWorkOrderList(obj) {
  return request({
    url: 'user/order/index',
    method: 'get',
    params: obj
  })
}
// 工单添加
export function addWorkOrder(obj) {
  return request({
    url: 'user/order/save',
    method: 'post',
    data: obj
  })
}
// 工单详情查看
export function workOrderInfo(obj) {
  return request({
    url: 'user/order/order_info',
    method: 'get',
    params: obj
  })
}
// 工单删除
export function deleteWorkOrder(obj) {
  return request({
    url: 'user/order/delete',
    method: 'post',
    data: obj
  })
}
// 客户联系人查询
export function getHotelContactList(obj) {
  return request({
    url: 'user/hotel/hotel_contact',
    method: 'get',
    params: obj
  })
}
// 客户联系人添加编辑
export function saveHotelContact(obj) {
  return request({
    url: 'user/hotel/hotel_contact_save',
    method: 'post',
    data: obj
  })
}
// 客户联系人删除
export function deleteHotelContact(obj) {
  return request({
    url: 'user/hotel/hotel_contact_delete',
    method: 'post',
    data: obj
  })
}

// 客户工作流程列表查询
export function getJobList(obj) {
  return request({
    url: 'user/hotel/hotel_wi',
    method: 'get',
    params: obj
  })
}
// 客户工作流程添加
export function saveJob(obj) {
  return request({
    url: 'user/hotel/hotel_wi_save',
    method: 'post',
    data: obj
  })
}
// 客户工作流程删除
export function deleteJob(obj) {
  return request({
    url: 'user/wi/delete',
    method: 'post',
    data: obj
  })
}
