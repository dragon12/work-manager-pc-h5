/** When your routing table is too long, you can split it into small modules **/

import Layout from '@/layout'

const workRouter = [
  // Client Management
  {
    path: '/client',
    component: Layout,
    redirect: '/client/index',
    name: 'Client Management',
    meta: {
      title: 'Client Management',
      icon: 'user'
    },
    children: [
      {
        path: 'index',
        component: () => import('@/views/client/index'),
        name: 'Client List',
        meta: { title: 'Client Management' }
      },
      {
        path: 'contact',
        component: () => import('@/views/client/contact'),
        name: 'Contact Person List',
        meta: { title: 'Contact Person List' }
      }
    ]
  }
  // Staff Management
]
export default workRouter
