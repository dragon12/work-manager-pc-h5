import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

/* Layout */
import Layout from '@/layout'
/**
 * Note: sub-menu only appear when route children.length >= 1
 * Detail see: https://panjiachen.github.io/vue-element-admin-site/guide/essentials/router-and-nav.html
 *
 * hidden: true                   if set true, item will not show in the sidebar(default is false)
 * alwaysShow: true               if set true, will always show the root menu
 *                                if not set alwaysShow, when item has more than one children route,
 *                                it will becomes nested mode, otherwise not show the root menu
 * redirect: noRedirect           if set noRedirect will no redirect in the breadcrumb
 * name:'router-name'             the name is used by <keep-alive> (must set!!!)
 * meta : {
    roles: ['admin','editor']    control the page roles (you can set multiple roles)
    title: 'title'               the name show in sidebar and breadcrumb (recommend set)
    icon: 'svg-name'             the icon show in the sidebar
    noCache: true                if set true, the page will no be cached(default is false)
    affix: true                  if set true, the tag will affix in the tags-view
    breadcrumb: false            if set false, the item will hidden in breadcrumb(default is true)
    activeMenu: '/example/list'  if set path, the sidebar will highlight the path you set
  }
 */

/**
 * constantRoutes
 * a base page that does not have permission requirements
 * all roles can be accessed
 */
export const constantRoutes = [
  {
    path: '/redirect',
    component: Layout,
    hidden: true,
    children: [
      {
        path: '/redirect/:path(.*)',
        component: () => import('@/views/redirect/index')
      }
    ]
  },
  {
    path: '/login',
    component: () => import('@/views/login/index'),
    hidden: true
  },
  {
    path: '/auth-redirect',
    component: () => import('@/views/login/auth-redirect'),
    hidden: true
  },
  {
    path: '/404',
    component: () => import('@/views/error-page/404'),
    hidden: true
  },
  {
    path: '/401',
    component: () => import('@/views/error-page/401'),
    hidden: true
  },
  {
    path: '/',
    component: Layout,
    redirect: '/main',
    children: [
      {
        path: 'main',
        component: () => import('@/views/main/index'),
        name: 'MainIndex',
        meta: { title: 'Dashboard', icon: 'dashboard', affix: true }
      }
    ]
  },
  {
    path: '/profile',
    component: Layout,
    redirect: '/profile/index',
    hidden: true,
    children: [
      {
        path: 'index',
        component: () => import('@/views/profile/index'),
        name: 'Profile',
        meta: { title: 'Profile', icon: 'user', noCache: true }
      }
    ]
  }
]

/**
 * asyncRoutes
 * the routes that need to be dynamically loaded based on user roles
 */
export const asyncRoutes = [
  {
    path: '/client',
    component: Layout,
    alwaysShow: true,
    name: 'Hotel',
    meta: { title: 'Client Management', icon: 'guide' },
    children: [
      {
        path: '/client/index',
        component: () => import('@/views/hotel/index'),
        name: 'hotel',
        meta: { title: 'Client List' }
      },
      {
        path: '/client/contact',
        component: () => import('@/views/contact/index'),
        name: 'contact',
        meta: { title: 'Contact Person List' }
      },
      {
        path: '/client/detail',
        component: () => import('@/views/hotel/detail/detailIndex'),
        name: 'hotelDetail',
        meta: { title: 'Client Details', public: true },
        hidden: true
      }
    ]
  },
  {
    path: '/order',
    component: Layout,
    alwaysShow: true,
    name: 'WorkOrder',
    meta: { title: 'Work Order', icon: 'edit' },
    children: [
      // {
      //   path: '/order/index',
      //   component: () => import('@/views/order/index'),
      //   name: 'workOrderStatus',
      //   meta: { title: 'Work Order Status' }
      // },
      {
        path: '/order/workorder',
        component: () => import('@/views/order/index'),
        name: 'WorkOrder',
        meta: { title: 'Work Order' }
      },
      {
        path: '/order/handyfixjob',
        component: () => import('@/views/order/index'),
        name: 'HandyfixJob',
        meta: { title: 'Handyfix Job' }
      },
      {
        path: '/order/orderdetails',
        component: () => import('@/views/order/orderdetails'),
        name: 'OrderDetails',
        meta: { title: 'Order Detail', public: true },
        hidden: true
      },
      {
        path: '/order/search.html',
        component: () => import('@/views/order/search'),
        name: 'OrderSearch',
        meta: { title: 'Order Report', public: true },
        hidden: true
      }
    ]
  },
  {
    path: '/advertising',
    component: Layout,
    alwaysShow: true,
    name: 'ad',
    meta: { title: 'Ad Management', icon: 'skill' },
    children: [
      {
        path: '/advertising/index',
        component: () => import('@/views/advertising/index'),
        name: 'advertisingSlot',
        meta: { title: 'Ad Position' }
      },
      {
        path: '/advertising/ad',
        component: () => import('@/views/advertising/ad'),
        name: 'advertising',
        meta: { title: 'Ad' },
        hidden: true
      }
    ]
  },
  {
    path: '/handyFix',
    component: Layout,
    alwaysShow: true,
    name: 'HandyFix',
    meta: { title: 'Handy Fix', icon: 'table' },
    children: [
      {
        path: '/handyFix/index',
        component: () => import('@/views/handyFix/index'),
        name: 'handyFix',
        meta: { title: 'Handy Fix Order' }
      },
      {
        path: '/handyFix/user',
        component: () => import('@/views/handyFix/user'),
        name: 'handyFixUser',
        meta: { title: 'Handy Fix User' }
      },
      {
        path: '/handyFix/refund',
        component: () => import('@/views/handyFix/refund'),
        name: 'handyFixRefund',
        meta: { title: 'Handy Fix Refund' }
      },
      {
        path: '/handyFix/orderdetails',
        component: () => import('@/views/handyFix/orderdetails'),
        name: 'OrderDetails',
        meta: { title: 'Order Detail', public: true },
        hidden: true
      }
    ]
  },
  {
    path: '/condominium',
    component: Layout,
    alwaysShow: true,
    name: 'Condominium',
    meta: { title: 'Condominium', icon: 'nested' },
    children: [
      {
        path: '/condominium/index',
        component: () => import('@/views/condominium/index'),
        name: 'condominium',
        meta: { title: 'Condominium' }
      }
    ]
  },
  {
    path: '/wi',
    component: Layout,
    alwaysShow: true,
    name: 'WorkInstruction',
    meta: { title: 'Work Instruction', icon: 'bug' },
    children: [
      {
        path: '/wi/index',
        component: () => import('@/views/instruction/index'),
        name: 'JobList',
        meta: { title: 'Job List' }
      },
      {
        path: '/wi/template',
        component: () => import('@/views/instruction/template'),
        name: 'TemplateList',
        meta: { title: 'Template List' }
      }
    ]
  },
  {
    path: '/staff',
    component: Layout,
    alwaysShow: true,
    name: 'User',
    meta: { title: 'Staff Management', icon: 'list' },
    children: [
      {
        path: '/staff/index',
        component: () => import('@/views/staff/index'),
        name: 'user',
        meta: { title: 'Staff List' }
      },
      {
        path: '/attence/index',
        component: () => import('@/views/attence/index'),
        name: 'AttenceList',
        meta: { title: 'Attendance' }
      }
    ]
  },
  {
    path: '/system',
    component: Layout,
    alwaysShow: true,
    name: 'Role',
    meta: { title: 'System Setting', icon: 'tab' },
    children: [
      {
        path: '/system/role',
        component: () => import('@/views/role/index'),
        name: 'role',
        meta: { title: 'Role Management' }
      }
    ]
  },
  {
    path: '/notification',
    component: Layout,
    alwaysShow: true,
    name: 'notification',
    meta: { title: 'Notification', icon: 'zip' },
    children: [
      {
        path: '/notification/index',
        component: () => import('@/views/notification/index'),
        name: 'NotificationList',
        meta: { title: 'Notification List' }
      }
    ]
  }
]

const createRouter = () => new Router({
  // mode: 'history', // require service support
  scrollBehavior: () => ({ y: 0 }),
  routes: constantRoutes
})

const router = createRouter()

// Detail see: https://github.com/vuejs/vue-router/issues/1234#issuecomment-357941465
export function resetRouter() {
  const newRouter = createRouter()
  router.matcher = newRouter.matcher // reset router
}

export default router
